var _currentUserId;
function agentStateEventHandler(agentState) {
    $('#agent_events').prepend(
            '<li class="list-group-item event-item"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(agentState) + '</code></pre></li>');
    $('#xuc_agentData').val("cause :  " + agentState.cause);
    if (agentState.name !== "AgentLoggedOut") {
        loginHandler(agentState);
    }
    switch (agentState.name) {
    case "AgentReady":
        readyHandler();
        break;
    case "AgentOnPause":
        onPauseHandler();
        break;
    case "AgentDialing":
        dialingHandler();
        break;
    case "AgentRinging":
        ringingHandler();
        break;
    case "AgentOnCall":
        onCallHandler(agentState);
        break;
    case "AgentOnWrapup":
        onWrapupHandler();
        break;
    case "AgentLoggedOut":
        logoutHandler();
        break;
    }
}
function loginHandler(event) {
    $('#xuc_agentStatus').val("Logged in");
    $('#xuc_agentPhoneNumber').val(event.phoneNb);
}

function logoutHandler() {
    $('#xuc_agentStatus').val("Logged out");
}

function readyHandler() {
    $('#xuc_agentStatus').val("Ready");
}
function dialingHandler() {
    $('#xuc_agentStatus').val("Dialing");
}
function ringingHandler() {
    $('#xuc_agentStatus').val("Ringing");
}

function onPauseHandler() {
    $('#xuc_agentStatus').val("Paused");
}
function onCallHandler(state) {
    $('#xuc_agentStatus').val("On Call");
    $('#xuc_agentData').val("Acd : " + state.acd + " dir : " + state.direction + " Type : " + state.callType);
    $('#xuc_agentMonitorState').val(state.monitorState);
}
function onWrapupHandler() {
    $('#xuc_agentStatus').val("Wrapup");
}
function usersStatusesHandler(statuses) {
    $("#userPresence").empty();
    $.each(statuses, function(key, item) {
        $("#userPresence")
                .append("<li><a href=\"#\" id=\"" + item.name + "\">" + item.longName + "</a></li>");
    });
    $('.dropdown-menu li a').click(function(event) {
        Cti.changeUserStatus($(this).attr('id'));
    });
}

function userConfigHandler(event) {
    console.log("user config Handler " + JSON.stringify(event));
    $('#xuc_userId').text(event.userId);
    _currentUserId = event.userId;
    $('#xuc_fullName').text(event.fullName);
    $('#xuc_mobileNumber').text(event.mobileNumber);
    $('#xuc_agentId').text(event.agentId);
    $('#xuc_voiceMailId').text(event.voiceMailId);
    $('#xuc_voiceMailEnabled').text(event.voiceMailEnabled);
    if (event.naFwdDestination !== null) {
        $('#xuc_naFwdEnabled').prop('checked', event.naFwdEnabled);
        $('#xuc_naFwdDestination').val(event.naFwdDestination);
    }
    if (event.uncFwdDestination !== null) {
        $('#xuc_uncFwdEnabled').prop('checked', event.uncFwdEnabled);
        $('#xuc_uncFwdDestination').val(event.uncFwdDestination);
    }

    if (event.busyFwdDestination !== null) {
        console.log("busy fwd " + event.busyFwdEnabled);
        $('#xuc_busyFwdEnabled').prop('checked', event.busyFwdEnabled);
        $('#xuc_busyFwdDestination').val(event.busyFwdDestination);
    }
}
function phoneStatusHandler(event) {
    console.log("phone Status Handler " + JSON.stringify(event));
    $('#xuc_phoneStatus').val(event.status);
}

function voiceMailStatusHandler(event) {
    $('#xuc_newMessages').text(event.newMessages);
    $('#xuc_waitingMessages').text(event.waitingMessages);
    $('#xuc_oldMessages').text(event.oldMessages);
}

function linkStatusHandler(event) {
    console.log("link Status Handler " + JSON.stringify(event));
    if (event.status === "closed") {
        $('#xuc_logon_panel').show();
    }
}

function loggedOnHandler() {
    console.log("Logged On Handler ");
    $('#xuc_logon_panel').hide();
}

function queueStatisticsHandler(event) {
    console.log("queue statistics Handler " + JSON.stringify(event));
    $('#queue_stats').prepend(
            '<li class="list-group-item"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</code></pre></li>');
}
function queueConfigHandler(queueConfig) {
    $('#queue_config').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(queueConfig) + '</code></pre></li>');
}
function queueMemberHandler(queueMember) {
    $('#queue_member').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(queueMember) + '</code></pre></li>');
}
function queueCallsHandler(queueCalls) {
    $('#queue_calls').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(queueCalls) + '</code></pre></li>');
}

function agentConfigHandler(agentConfig) {
    console.log("agent config Handler " + JSON.stringify(agentConfig));
    $('#agent_config').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(agentConfig) + '</code></pre></li>');
}
function agentGroupConfigHandler(agentGroups) {
    console.log("agent group config Handler " + JSON.stringify(agentGroups));
    $('#agentgroup_config').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(agentGroups) + '</code></pre></li>');
}

function agentErrorHandler(error) {
    generateMessage(error, true);
}
function agentStatisticsHandler(event) {
    console.log("agent statistics Handler " + JSON.stringify(event));
    $('#agent_stats').prepend(
            '<li class="list-group-item  event-item"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</code></pre></li>');
}
function errorHandler(error) {
    generateMessage(error, true);
}
function agentDirectoryHandler(agentDirectory) {
    console.log("agent directory Handler " + JSON.stringify(agentDirectory));
    $('#agent_directory').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(agentDirectory) + '</code></pre></li>');
}
function agentListenHandler(listenEvent) {
    $('#xuc_agentListenStatus').val(JSON.stringify(listenEvent));
}
function callHistoryHandler(callHistory) {
    console.log('Call history handler ' + JSON.stringify(callHistory));
    $('#call_history').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(callHistory) + '</code></pre></li>');
}
function callbacksHandler(callbackLists) {
    console.log('Callback lists handler' + JSON.stringify(callbackLists));
    $('#callbacks').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(callbackLists) + '</code></pre></li>');
}
function phoneEventHandler(event) {
    console.log('Phone Event handler' + JSON.stringify(event));
    $('#phone_events').prepend('<li class="list-group-item event-item"><pre><code>' + JSON.stringify(event) + '</code></pre></li>');
    $('#last_phone_event').html(
                 '<p class="text-normal"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</code></pre></p>');

}
function webRtcGeneralEventHandler(event) {
    console.log('WebRTC Event handler' + JSON.stringify(event));
    $('#webrtc_events').prepend('<li class="list-group-item event-item"><pre><code>GeneralEvent: ' + JSON.stringify(event) + '</code></pre></li>');
}
function webRtcRegistrationEventHandler(event) {
    console.log('WebRTC Event handler' + JSON.stringify(event));
    $('#webrtc_events').prepend('<li class="list-group-item event-item"><pre><code>RegistrationEvent: ' + JSON.stringify(event) + '</code></pre></li>');
}
function webRtcIncomingEventHandler(event) {
    console.log('WebRTC Event handler' + JSON.stringify(event));
    $('#webrtc_events').prepend('<li class="list-group-item event-item"><pre><code>IncomingEvent: ' + JSON.stringify(event) + '</code></pre></li>');
}
function webRtcOutgoingEventHandler(event) {
    console.log('WebRTC Event handler' + JSON.stringify(event));
    $('#webrtc_events').prepend('<li class="list-group-item event-item"><pre><code>OutgoingEvent: ' + JSON.stringify(event) + '</code></pre></li>');
}

function membershipEventHandler(event) {
    console.log('UserQueueDefaultMembership Event handler ' + JSON.stringify(event));
    $('#xuc_membership_event').prepend('<li class="list-group-item event-item"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</code></pre></li>');
}

function init(username, password, phoneNumber, wsurl) {
  Cti.debugMsg = true;
  Cti.debugHandler = false;
  Cti.clearHandlers();
  Cti.setHandler(Cti.MessageType.LOGGEDON, loggedOnHandler);
  //user
  Cti.setHandler(Cti.MessageType.USERSTATUSES, usersStatusesHandler);
  Cti.setHandler(Cti.MessageType.USERCONFIGUPDATE, userConfigHandler);

  Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, phoneStatusHandler);
  Cti.setHandler(Cti.MessageType.VOICEMAILSTATUSUPDATE, voiceMailStatusHandler);
  Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, linkStatusHandler);

  // Agent
  Cti.setHandler(Cti.MessageType.AGENTCONFIG, agentConfigHandler);
  Cti.setHandler(Cti.MessageType.AGENTDIRECTORY, agentDirectoryHandler);
  Cti.setHandler(Cti.MessageType.AGENTERROR, agentErrorHandler);
  Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, agentGroupConfigHandler);
  Cti.setHandler(Cti.MessageType.AGENTLIST, agentConfigHandler);
  Cti.setHandler(Cti.MessageType.AGENTLISTEN, agentListenHandler);
  Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, agentStateEventHandler);
  Cti.setHandler(Cti.MessageType.AGENTSTATISTICS, agentStatisticsHandler);

  //Queue
  Cti.setHandler(Cti.MessageType.QUEUECONFIG, queueConfigHandler);
  Cti.setHandler(Cti.MessageType.QUEUELIST, queueConfigHandler);
  Cti.setHandler(Cti.MessageType.QUEUEMEMBER, queueMemberHandler);
  Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, queueMemberHandler);
  Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, queueStatisticsHandler);
  Cti.setHandler(Cti.MessageType.QUEUECALLS, queueCallsHandler);

  Cti.setHandler(Cti.MessageType.ERROR, errorHandler);

  Cti.setHandler(Cti.MessageType.CALLHISTORY, callHistoryHandler);
  Cti.setHandler(Cti.MessageType.CUSTOMERCALLHISTORY, callHistoryHandler);

  Cti.setHandler(Callback.MessageType.CALLBACKLISTS, callbacksHandler);

  Cti.setHandler(Cti.MessageType.PHONEEVENT, phoneEventHandler);
  Cti.setHandler(Cti.MessageType.CURRENTCALLSPHONEEVENTS, phoneEventHandler);

  Cti.setHandler(Membership.MessageType.USERQUEUEDEFAULTMEMBERSHIP, membershipEventHandler);

  xc_webrtc.clearHandlers();
  xc_webrtc.setHandler(xc_webrtc.MessageType.GENERAL, webRtcGeneralEventHandler);
  xc_webrtc.setHandler(xc_webrtc.MessageType.REGISTRATION, webRtcRegistrationEventHandler);
  xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, webRtcIncomingEventHandler);
  xc_webrtc.setHandler(xc_webrtc.MessageType.OUTGOING, webRtcOutgoingEventHandler);
  xc_webrtc.disableICE();
  xc_webrtc.stop();

  Cti.WebSocket.init(wsurl, username, phoneNumber);
  Callback.init(Cti);
  Membership.init(Cti);

  // sampleContacts.js
  initContacts();

  // sampleConferences.js
  initConferences();
}

function generateMessage(message, isError) {
  var className = (isError) ? 'danger' : 'success';
  $('#main_errors').html('<p class="alert alert-'+className+' alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + new Date().toLocaleString() + ": " + JSON.stringify(message) + '</p>');
  console.log(message);
}

$(function() {
    console.log("sample handlers - main init");
    $('#xuc_webrtc_video').hide();

    var xucserver = window.location.hostname+":"+window.location.port;
    $('#xuc_server').val(xucserver);

    $('#xuc_username').val(localStorage.getItem('sampleUsername'));
    $('#xuc_password').val(localStorage.getItem('samplePassword'));
    $('#xuc_phoneNumber').val(localStorage.getItem('samplePhoneNumber'));

    $('#xuc_sign_in').click(function(event) {
      var server = $('#xuc_server').val();
      var username = $('#xuc_username').val();
      var password = $('#xuc_password').val();
      var phoneNumber = $('#xuc_phoneNumber').val();

      localStorage.setItem('sampleUsername', username);
      localStorage.setItem('samplePassword', password);
      localStorage.setItem('samplePhoneNumber', phoneNumber);
      var wsProto = window.location.protocol === "http:" ? 'ws://' : 'wss://';

      console.log("sign in " + server + " : " + username + " " + password + " " + phoneNumber);
      makeAuthCall("POST", "/xuc/api/2.0/auth/login",
       {"login": username, "password": password}).success(function(data){
          $('#xuc_token').val(data.token);
          init(username, password, phoneNumber, wsProto + server + "/xuc/api/2.0/cti?token=" + data.token);
        }).fail(function(sender, message, details){
          var error = "Impossible to retrieve auth token for user " + username;
          generateMessage(error, true, true);
      });
    });
    $('#xuc_restart').click( function(event) {
        Cti.close();
        var server = $('#xuc_server').val();
        var username = $('#xuc_username').val();
        var password = $('#xuc_password').val();
        var phoneNumber = $('#xuc_phoneNumber').val();
        var token = $('#xuc_token').val();
        var wsProto = window.location.protocol === "http:" ? 'ws://' : 'wss://';
        var wsurl = wsProto + server + "api/2.0/cti?token=" + token;
        Cti.WebSocket.init(wsurl, username, phoneNumber);
    });



    $('#xuc_login_btn').click(function(event) {
        Cti.loginAgent($('#xuc_agentPhoneNumber').val());
    });
    $('#xuc_logout_btn').click(function(event) {
        Cti.logoutAgent();
    });
    $('#xuc_pause_btn').click(function(event) {
        Cti.pauseAgent(undefined, $('#agent_pause_reason').val());
    });
    $('#xuc_unpause_btn').click(function(event) {
        Cti.unpauseAgent();
    });
    $('#xuc_subscribe_to_queue_stats_btn').click(function(event) {
        Cti.subscribeToQueueStats();
    });
    $('#xuc_answer_btn').click(function(event) {
        Cti.answer();
    });
    $('#xuc_hangup_btn').click(function(event) {
        Cti.hangup();
    });
    $('#xuc_search_btn').click(function(event) {
        Cti.searchDirectory($("#xuc_destination").val());
    });
    $('#xuc_enable_dnd_btn').click(function(event) {
        Cti.dnd(true);
    });
    $('#xuc_disable_dnd_btn').click(function(event) {
        Cti.dnd(false);
    });
    $('#xuc_dial_btn').click(function(event) {
        var variables = extractVariables($("#xuc_dial_variables").val());
        Cti.dial($("#xuc_destination").val(), variables);
    });
    $('#xuc_hold_btn').click(function(event) {
        Cti.hold();
    });
    $('#xuc_dial_mobile_btn').click(function(event) {
        var variables = extractVariables($("#xuc_dial_variables").val());
        Cti.dialFromMobile($("#xuc_destination").val(), variables);
    });
    $('#xuc_setdata_btn').click(function(event) {
        var variables = extractVariables($("#xuc_dial_variables").val());
        Cti.setData(variables);
    });
    $('#xuc_direct_transfer_btn').click(function(event) {
        Cti.directTransfer($("#xuc_destination").val());
    });
    $('#xuc_attended_transfer_btn').click(function(event) {
        Cti.attendedTransfer($("#xuc_destination").val());
    });
    $('#xuc_complete_transfer_btn').click(function(event) {
        Cti.completeTransfer();
    });
    $('#xuc_cancel_transfer_btn').click(function(event) {
        Cti.cancelTransfer();
    });
    $('#xuc_monitorpause_btn').click(function(event) {
        Cti.monitorPause(parseInt($('#xuc_agentId').text()));
    });
    $('#xuc_monitorunpause_btn').click(function(event) {
        Cti.monitorUnpause(parseInt($('#xuc_agentId').text()));
    });
    $('#xuc_get_config_queues').click(function(event) {
        $('#queue_config').html("");
        Cti.getList("queue");
    });
    $('#xuc_queues_recording_on').click(function(event) {
      makeAuthCall('POST', "/xuc/api/2.0/config/recording/status/on",{});
    });
    $('#xuc_queues_recording_off').click(function(event) {
      makeAuthCall('POST', "/xuc/api/2.0/config/recording/status/off",{});
    });
    $('#xuc_get_config_queuemembers').click(function(event) {
        Cti.getList("queuemember");
    });
    $('#xuc_subscribe_to_queue_calls').click(function(event) {
        Cti.subscribeToQueueCalls(parseInt($('#xuc_queue_id_calls').val()));
    });
    $('#xuc_unsubscribe_to_queue_calls').click(function(event) {
        Cti.unSubscribeToQueueCalls(parseInt($('#xuc_queue_id_calls').val()));
    });
    $('#xuc_get_config_agents').click(function(event) {
        Cti.getList("agent");
    });
    $('#xuc_get_config_agentgroups').click(function(event) {
        Cti.getList("agentgroup");
    });
    $('#xuc_get_agentstates').click(function(event) {
        Cti.getAgentStates();
    });
    $('#xuc_clean_queue_stats').click(function(event) {
         $('#queue_stats').empty();
    });
    $('#xuc_clean_phone_events').click(function(event) {
         $('#phone_events').empty();
    });
    $('#xuc_clean_agent_events').click(function(event) {
         $('#agent_events').empty();
         $('#agentstates').empty();
    });
    $('#xuc_clean_queue_config').click(function(event) {
         $('#queue_config').empty();
    });

    $('#xuc_clean_queue_member').click(function(event) {
         $('#queue_member').empty();
    });
    $('#xuc_clean_queue_calls').click(function(event) {
         $('#queue_calls').empty();
    });
    $('#xuc_clean_agent_config').click(function(event) {
         $('#agent_config').empty();
    });
    $('#xuc_clean_agent_directory').click(function(event) {
         $('#agent_directory').empty();
    });
    $('#xuc_clean_agent_statistic').click(function(event) {
        $('#agent_stats').empty();
    });
    $('#xuc_clean_agentgroup_config').click(function(event) {
         $('#agentgroup_config').empty();
    });
    $('#xuc_clean_call_history').click(function(event) {
         $('#call_history').empty();
    });

    $('#xuc_get_current_calls_phone_events').click(function(event) {
        Cti.getCurrentCallsPhoneEvents();
    });
    $('#xuc_subscribe_to_agent_event_btn').click(function(event) {
        Cti.subscribeToAgentEvents();
    });
    $('#xuc_subscribe_to_agent_statistic_btn').click(function(event) {
        Cti.subscribeToAgentStats();
    });
    $('#xuc_get_agent_directory').click(function(event) {
        Cti.getAgentDirectory();
    });
    $('#xuc_set_agent_queue_btn').click(function(event) {
        Cti.setAgentQueue($("#xuc_agentId_target").val(), $("#xuc_queueId").val(), $("#xuc_penalty").val());
    });
    $('#xuc_remove_agent_from_queue_btn').click(function(event) {
        Cti.removeAgentFromQueue($("#xuc_agentId_target").val(),$("#xuc_queueId").val());
    });
    $('#xuc_listen_agent_btn').click(function(event) {
        Cti.listenAgent(parseInt($("#xuc_agentId_target").val()));
    });
    $('#xuc_naFwdEnabled').click(function() {
        console.log($(this).is(':checked'));
        Cti.naFwd($("#xuc_naFwdDestination").val(),$(this).is(':checked'));
        $(this).prop('checked', !($(this).is(':checked')));
    });
    $('#xuc_uncFwdEnabled').click(function() {
        console.log($(this).is(':checked'));
        Cti.uncFwd($("#xuc_uncFwdDestination").val(),$(this).is(':checked'));
        $(this).prop('checked', !($(this).is(':checked')));
    });
    $('#xuc_busyFwdEnabled').click(function() {
        console.log($(this).is(':checked'));
        Cti.busyFwd($("#xuc_busyFwdDestination").val(),$(this).is(':checked'));
        $(this).prop('checked', !($(this).is(':checked')));
    });
    $('#xuc_get_agent_call_history').click(function() {
        Cti.getAgentCallHistory(7);
    });
    $('#xuc_find_customer_call_history').click(function() {
        Cti.findCustomerCallHistory(1, [{"field": $("#xuc_cus_field").val(), "operator": $("#xuc_cus_operator").val(), "value": $("#xuc_cus_value").val()}], 7);
    });
    $('#xuc_get_user_call_history').click(function() {
        Cti.getUserCallHistory(7);
    });
    $('#xuc_get_queue_call_history').click(function() {
        Cti.getQueueCallHistory($("#xuc_queue").val(), 7);
    });
    $('#xuc_clean_callbacks').click(function (){
        $('#callbacks').empty();
    });
    $('#xuc_get_callback_lists').click(function() {
        Callback.getCallbackLists();
    });

    $('#xuc_clean_webrtc_events').click(function(event) {
         $('#webrtc_events').empty();
    });
    $('#xuc_init_webrtc').click(function() {
        if (window.location.hostname === "127.0.0.1" || window.location.hostname === "localhost") {
            xc_webrtc.init($('#xuc_fullName').text(), false, 5039);
        } else {
            var wss = window.location.protocol === "http:" ? false : true;
            xc_webrtc.init($('#xuc_fullName').text(), wss, 443, 'audio_remote', window.location.hostname);
        }
    });
    $('#xuc_stop_webrtc').click(function() {
        xc_webrtc.stop();
    });
    $('#xuc_webrtc_dial').click(function() {
        xc_webrtc.dial($('#xuc_webrtc_dst').val());
    });
    $('#xuc_webrtc_dial_video').click(function() {
        xc_webrtc.dial($('#xuc_webrtc_dst').val(), true);
        $('#xuc_webrtc_video').show();
    });
    $('#xuc_webrtc_dst').keypress(function(e) {
        if (e.which == $.ui.keyCode.ENTER) {
            xc_webrtc.dial($('#xuc_webrtc_dst').val());
        }
    });
    $('#xuc_webrtc_answer').click(function() {
        xc_webrtc.answer();
    });
    $('#xuc_webrtc_hangup').click(function() {
        $('#xuc_webrtc_video').hide();
        Cti.hangup();
    });
    $('#xuc_webrtc_send_dtmf').click(function() {
        xc_webrtc.dtmf($('#xuc_webrtc_dtmf').val());
    });
    $('#xuc_webrtc_dtmf').keypress(function(e) {
        if (e.which == $.ui.keyCode.ENTER) {
            xc_webrtc.dtmf($('#xuc_webrtc_dtmf').val());
        }
    });
    $('#xuc_webrtc_hold').click(function() {
        xc_webrtc.hold();
    });
    $('#xuc_webrtc_attended_transfer_btn').click(function() {
        xc_webrtc.attendedTransfer($('#xuc_webrtc_dst').val());
    });
    $('#xuc_webrtc_complete_transfer_btn').click(function() {
        xc_webrtc.completeTransfer();
    });
    $('#xuc_dial_from_queue_btn').click(function(event) {
        var variables = extractVariables($("#xuc_dial_from_variables").val());
        Cti.dialFromQueue(
            $("#xuc_dial_from_queue_destination").val(),
            $("#xuc_dial_from_queue_id").val(),
            $("#xuc_dial_from_queue_caller_id_name").val(),
            $("#xuc_dial_from_queue_caller_id_number").val(),
            variables
        );
    });

    $('#xuc_membership_clean').click(function() {
        $('#xuc_membership_event').html("");
    });

    $('#xuc_membership_get').click(function() {
        Membership.getUserDefaultMembership(_currentUserId);
    });

    $('#xuc_membership_set').click(function() {
        var membership = $("#xuc_membership_data").val();
        Membership.setUserDefaultMembership(_currentUserId, JSON.parse(membership));
    });

    $('#xuc_membership_apply').click(function() {
        var userIds = $("#xuc_membership_users").val();
        Membership.applyUsersDefaultMembership(JSON.parse(userIds));
    });
});
