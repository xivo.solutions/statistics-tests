// Karma configuration
// Generated on Wed Jul 31 2019 10:01:07 GMT+0200 (GMT+02:00)

var path = require('path');


module.exports = function(config) {

  config.test = config.test ? config.test : 'test/echo_test';
  config.user = config.user ? config.user : '1-caller';


  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'browserify'],


    client: {
        jasmine: {
            random: false,
            timeoutInterval: 10000
        },
        captureConsole: true
    },

    // list of files / patterns to load in the browser
    files: [
      'node_modules/jquery/dist/jquery.js',
      'public/SIPml-api.js',
      'public/shotgun.js',
      'assets/*.js',
      path.join(config.test, config.user + ".js")
    ],


    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        'test/**/*.js': ['webpack']
    },
    webpack: {
        module: {
            rules: [
                {
                    test: /\.js?$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: 'babel-loader',
                    query: {
                        presets: ['es2015'],
                        plugins: []
                    }
                }
            ]
        },
        watch: true,
        mode: 'development'
    },
    webpackServer: {
        noInfo: true
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['verbose', 'specjson'],


    specjsonReporter: {
        outputFile: path.join("results", path.basename(config.test), config.user + ".json")
    },
//    jsonReporter: {
//      stdout: false,
//      outputFile: 'results/results.json' // defaults to none
//    },

//    logReporter: {
//      outputPath: "results/", // default name is current directory
//      logFileName: "results.log" // default name is logFile_month_day_year_hr:min:sec.log
//    },

//    jsonResultReporter: {
//      outputFile: "results/karma-result.json",
//      isSynchronous: false //(optional, default false)
//    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['ChromeUsingFakeUI'],

    customLaunchers: {
      ChromeUsingFakeUI: {
        base: 'Chrome',
        flags: ['--use-fake-ui-for-media-stream']
      }
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
