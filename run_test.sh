#!/bin/bash


function getUsers {
    local scenario=$1
    find $scenario/*.js -type f -exec basename {} .js \;
}


test=$1

if [ -z "$test" ]; then
    echo "Usage: ./run_test.sh path_to_test_dir"
    exit 1
elif [ ! -d "$test" ]; then
    echo "Directory $test does not exist"
    exit 1
fi

users=$(getUsers $test)
resultsPath=results/$(basename $test)
mkdir -p $resultsPath
declare -a options

for user in $users; do
    echo Running $test / $user
    karma start karma.config.js --test=$test --user=$user > $resultsPath/$user &
    sleep 0.2
    options+=(" -cT ansi $resultsPath/$user")
done

multitail ${options[@]}
