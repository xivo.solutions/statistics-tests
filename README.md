# statistics-tests

## Setup PC

    npm i
    sudo npm install -g karma-cli
    sudo apt-get install multitail

## Setup XiVO

Import users.csv
Create agent group "statistics"
Create agents "webrtc statistics A1..A3" from users A1..A3
Create queues "statistics A1..A3"
Add one agent to each queue

## Test

Echo test scenario:

    npm test

Single scenario with one participant:

    karma start karma.config.js --test=test/echo_test --user=1-caller

Single scenario with all participants:

    ./run_test.sh test/echo_test

Suite:

    ./run_suite.sh
