export class EventHelper {

    constructor(debug = false) {

        this.webRtcGeneralEventStack = [];
        this.webRtcRegistrationEventStack = [];
        this.webRtcIncomingEventStack = [];
        this.webRtcOutgoingEventStack = [];

        this.registerWebRtcHandlers();

        if (debug) {
            xc_webrtc.setDebug('debug', true, true);
            Cti.debugMsg = true;
        }
    }

    webRtcGeneralEventHandler(event) {
        console.log('webRtcGeneralEventHandler: ' + this.webRtcGeneralEventStack);
        this.webRtcGeneralEventStack.push(JSON.stringify(event));
        if (event.type === xc_webrtc.General.FAILED) {
            onFatalWebRtcError('UNABLE_TO_START_WEBRTC');
            CtiProxy.stopWebRtc();
        }
    }

    webRtcRegistrationEventHandler(event) {
        console.log('webRtcRegistrationEventHandler: ' + this.webRtcRegistrationEventStack);
        this.webRtcRegistrationEventStack.push(JSON.stringify(event));
        if (event.type === xc_webrtc.Registration.UNREGISTERED) {
            onFatalWebRtcError('UNABLE_TO_REGISTER_WEBRTC');
            CtiProxy.stopWebRtc();
        }
    };

    webRtcIncomingEventHandler(event) {
        console.log('webRtcIncomingEventHandler: ' + this.webRtcIncomingEventStack);
        this.webRtcIncomingEventStack.push(JSON.stringify(event));
    };

    webRtcOutgoingEventHandler(event) {
        console.log('webRtcOutgoingEventHandler: ' + this.webRtcOutgoingEventStack);
        this.webRtcOutgoingEventStack.push(JSON.stringify(event));
    };

    registerWebRtcHandlers() {

        xc_webrtc.clearHandlers();
        xc_webrtc.setHandler(xc_webrtc.MessageType.GENERAL, this.webRtcGeneralEventHandler.bind(this));
        xc_webrtc.setHandler(xc_webrtc.MessageType.REGISTRATION, this.webRtcRegistrationEventHandler.bind(this));
        xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, this.webRtcIncomingEventHandler.bind(this));
        xc_webrtc.setHandler(xc_webrtc.MessageType.OUTGOING, this.webRtcOutgoingEventHandler.bind(this));
        xc_webrtc.disableICE();
    }

    waitForEventType(eventStackName, eventType) {
        return new Promise((resolve) => {
            let re = new RegExp('{"type":"' + eventType + '"');
            let stack;
            let _waitForEventType = this.waitForEventType.bind(this);

            switch(eventStackName) {
              case "webRtcRegistrationEventStack":
                stack = this.webRtcRegistrationEventStack;
                break;
              case "webRtcGeneralEventStack":
                stack = this.webRtcGeneralEventStack;
                break;
              case "webRtcIncomingEventStack":
                stack = this.webRtcIncomingEventStack;
                break;
              case "webRtcOutgoingEventStack":
                stack = this.webRtcOutgoingEventStack;
                break;
            }

            if (stack.findIndex(value => re.test(value)) < 0) {
                setTimeout(function () {
                    return resolve(_waitForEventType(eventStackName, eventType));
                }, 10)
            } else {
                return resolve()
            }
        })
    }

}
