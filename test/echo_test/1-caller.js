import {EventHelper} from '../EventHelper';
import {Config} from '../Config';


let debug = false;
var eventHelper = new EventHelper(debug);
var config = new Config();

var me = config.U1;


describe("Echo test", function() {

  it("register", async function () {
    xc_webrtc.initByLineConfig(me.lineCfg, me.username, me.usingSsl, me.port, 'audio_remote', me.lineCfg.xivoIp);

    const result = await eventHelper.waitForEventType("webRtcRegistrationEventStack", "Registered");

    expect(eventHelper.webRtcRegistrationEventStack.toString()).toBe('{"type":"Registered"}');
  });

  it("dial echo test", async function() {
    xc_webrtc.dial("*55");

    const result = await eventHelper.waitForEventType("webRtcOutgoingEventStack", "Establishing");

    expect(eventHelper.webRtcOutgoingEventStack.indexOf('{"type":"Establishing"}')).toBeGreaterThanOrEqual(0);
  });

  it("is connected", async function() {
    const result = await eventHelper.waitForEventType("webRtcOutgoingEventStack", "Connected");

    let regex = new RegExp('{"type":"Connected","data":{"callee":"\\*55"}');
    expect(eventHelper.webRtcOutgoingEventStack.findIndex(value => regex.test(value))).toBeGreaterThanOrEqual(0);
  });

  it("listen 1 s", function(done) {
    setTimeout(function() {
      done();
    }, 1000);
  });

  it("hang up", function(done) {
    console.log("webRtcOutgoingEventStack: " + eventHelper.webRtcOutgoingEventStack);
    xc_webrtc.stop();
    done();
  });

  it("unregister", function(done) {
    let sessions = xc_webrtc.allButWithId(-1);
    if (sessions.length > 0) {
      sessions[0].session.hangup();
    }
    done();
  });

});
