const hasDevice = false
const webRtc = true
const xivoIp = '192.168.227.10'

const lineCfgA1 = {
    name: '1pvae2a6',
    password: 'w3rtocle'
};

const lineCfgA2 = {
    name: 'rs55m4te',
    password: '8pmp8l0q'
};

const lineCfgA3 = {
    name: 't0xueyex',
    password: 'x80z4q39'
};

const lineCfgU1 = {
    name: 'hqd7hqa5',
    password: 'uj1yftfy'
};

const lineCfgU2 = {
    name: 'vhsnbek0',
    password: 's5r2slxu'
};


class User {

    constructor(name, lineCfg, number, queueNumber) {
        this.usingSsl = false;
        this.port = '5039';
        this.username = 'webrtc statistics ' + name;

        this.lineCfg = lineCfg;
        this.lineCfg.hasDevice = hasDevice;
        this.lineCfg.webRtc = webRtc;
        this.lineCfg.xivoIp = xivoIp;

        this.number = number.toString();
        this.queueNumber = queueNumber ? queueNumber.toString() : null;
    }
}

export class Config {

    constructor() {
        this.A1 = new User("A1", lineCfgA1, 1230, 3300);
        this.A2 = new User("A2", lineCfgA2, 1231, 3301);
        this.A3 = new User("A3", lineCfgA3, 1232, 3302);
        this.U1 = new User("U1", lineCfgU1, 1233, null);
        this.U2 = new User("U2", lineCfgU2, 1234, null);
    }
}
