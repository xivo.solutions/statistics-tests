import {EventHelper} from '../EventHelper';
import {Config} from '../Config';


let debug = false;
var eventHelper = new EventHelper(debug);
var config = new Config();

var agent = config.A1;
var destination = config.A2;
var caller = config.U1;
var me = caller;


describe("Call queue", function() {

  it("wait 2 s for agents logged in", function(done) {
    setTimeout(function() {
      done();
    }, 2000);
  });

  it("register", async function() {
    xc_webrtc.initByLineConfig(me.lineCfg, me.username, me.usingSsl, me.port, 'audio_remote', me.lineCfg.xivoIp);

    const result = await eventHelper.waitForEventType("webRtcRegistrationEventStack", "Registered");

    expect(eventHelper.webRtcRegistrationEventStack.toString()).toBe('{"type":"Registered"}');
  });

  it("dial " + agent.queueNumber, async function() {
    xc_webrtc.dial(agent.queueNumber);

    const result = await eventHelper.waitForEventType("webRtcOutgoingEventStack", "Establishing");

    expect(eventHelper.webRtcOutgoingEventStack.indexOf('{"type":"Establishing"}')).toBeGreaterThanOrEqual(0);
  });

  it("is connected", async function() {
    const result = await eventHelper.waitForEventType("webRtcOutgoingEventStack", "Connected");

    let regex = new RegExp('{"type":"Connected","data":{"callee":"' + agent.queueNumber + '"}');
    expect(eventHelper.webRtcOutgoingEventStack.findIndex(value => regex.test(value))).toBeGreaterThanOrEqual(0);
  });

  it("wait 2 s (on hold)", function(done) {
    setTimeout(function() {
      done();
    }, 2000);
  });

  it("is terminated", async function() {
    const result = await eventHelper.waitForEventType("webRtcOutgoingEventStack", "Terminated");

    let regex = new RegExp('{"type":"Terminated","data":{"reason":"Call terminated"}');
    expect(eventHelper.webRtcOutgoingEventStack.findIndex(value => regex.test(value))).toBeGreaterThanOrEqual(0);
  });

  it("hang up", function(done) {
    console.log("webRtcOutgoingEventStack: " + eventHelper.webRtcOutgoingEventStack);
    xc_webrtc.stop();
    done();
  });

  it("unregister", function(done) {
    let sessions = xc_webrtc.allButWithId(-1);
    if (sessions.length > 0) {
      sessions[0].session.hangup();
    }
    done();
  });

});
