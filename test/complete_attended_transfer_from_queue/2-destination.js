import {EventHelper} from '../EventHelper';
import {Config} from '../Config';


let debug = false;
var eventHelper = new EventHelper(debug);
var config = new Config();

var agent = config.A1;
var destination = config.A2;
var caller = config.U1;
var me = destination;


describe("Answer, ", function() {

  it("register", async function() {
    xc_webrtc.initByLineConfig(me.lineCfg, me.username, me.usingSsl, me.port, 'audio_remote', me.lineCfg.xivoIp);

    const result = await eventHelper.waitForEventType("webRtcRegistrationEventStack", "Registered");

    expect(eventHelper.webRtcRegistrationEventStack.toString()).toBe('{"type":"Registered"}');
  });

  it("agent login", async function() {
    xc_webrtc.dial("*31" + me.number);

    const result = await eventHelper.waitForEventType("webRtcOutgoingEventStack", "Establishing");

    expect(eventHelper.webRtcOutgoingEventStack.indexOf('{"type":"Establishing"}')).toBeGreaterThanOrEqual(0);
  });

  it("agent logged", async function() {
    const result = await eventHelper.waitForEventType("webRtcOutgoingEventStack", "Connected");

    let regex = new RegExp('{"type":"Connected","data":{"callee":"\\*31' + me.number + '"}');
    expect(eventHelper.webRtcOutgoingEventStack.findIndex(value => regex.test(value))).toBeGreaterThanOrEqual(0);
  });

  it("wait 3 s to sync with others", function(done) {
    setTimeout(function() {
      done();
    }, 3000);
  });

  it("is ringing", async function() {
    const result = await eventHelper.waitForEventType("webRtcIncomingEventStack", "Setup");

    let regex = new RegExp('{"type":"Setup","data":{"caller":"' + agent.number + '"}');
    expect(eventHelper.webRtcIncomingEventStack.findIndex(value => regex.test(value))).toBeGreaterThanOrEqual(0);
  });

  it("answer", function(done) {
    xc_webrtc.answer();
    done();
  });

  it("talk 1 s with agent and 2 s with caller", function(done) {
    setTimeout(function() {
      done();
    }, 3000);
  });

  it("hang up", function(done) {
    console.log("webRtcOutgoingEventStack: " + eventHelper.webRtcOutgoingEventStack);
    console.log("webRtcIncomingEventStack: " + eventHelper.webRtcIncomingEventStack);
    xc_webrtc.stop();
    done();
  });

  it("unregister", function(done) {
    let sessions = xc_webrtc.allButWithId(-1);
    if (sessions.length > 0) {
      sessions[0].session.hangup();
    }
    done();
  });

});
