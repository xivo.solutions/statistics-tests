import {EventHelper} from '../EventHelper';
import {Config} from '../Config';


let debug = false;
var eventHelper = new EventHelper(debug);
var config = new Config();

var agent = config.A1;
var caller = config.U1;
var me = agent;


describe("Answer and hangup", function() {

  it("register", async function() {
    xc_webrtc.initByLineConfig(me.lineCfg, me.username, me.usingSsl, me.port, 'audio_remote', me.lineCfg.xivoIp);

    const result = await eventHelper.waitForEventType("webRtcRegistrationEventStack", "Registered");

    expect(eventHelper.webRtcRegistrationEventStack.toString()).toBe('{"type":"Registered"}');
  });

  it("is ringing", async function() {
    const result = await eventHelper.waitForEventType("webRtcIncomingEventStack", "Setup");

    let regex = new RegExp('{"type":"Setup","data":{"caller":"' + caller.number + '"}');
    expect(eventHelper.webRtcIncomingEventStack.findIndex(value => regex.test(value))).toBeGreaterThanOrEqual(0);
  });

  it("answer", function(done) {
    xc_webrtc.answer();
    done();
  });

  it("talk 1 s", function(done) {
    setTimeout(function() {
      done();
    }, 1000);
  });

  it("hang up", function(done) {
    console.log("webRtcIncomingEventStack: " + eventHelper.webRtcIncomingEventStack);
    xc_webrtc.stop();
    done();
  });

  it("unregister", function(done) {
    let sessions = xc_webrtc.allButWithId(-1);
    if (sessions.length > 0) {
      sessions[0].session.hangup();
    }
    done();
  });

});
