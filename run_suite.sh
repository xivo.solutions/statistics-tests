#!/bin/bash


function getTests {
    find test/* -type d -exec basename {} \;
}

function getUsers {
    local scenario=$1
    find test/$scenario/*.js -type f -exec basename {} .js \;
}


tests=$(getTests)
failedTotal=0

for test in $tests; do
    declare -a pids
    users=$(getUsers $test)
    resultsPath=results/$test
    mkdir -p $resultsPath
    echo -ne "\n$test:"

    for user in $users; do
        echo -n " $user"
        karma start karma.config.js --test=test/$test --user=$user > $resultsPath/$user &
        pid=$!
        pids+=($pid)

        sleep 0.2
    done
    echo

    wait "${pids[@]}"
    unset pids

    failedUsers=0
    for user in $users; do
        passed=$(grep 'PASSED' $resultsPath/$user.json | wc -l)
        failed=$(grep 'FAILED' $resultsPath/$user.json | wc -l)
        failedUsers=$(($failedUsers + $failed))
        echo -e "    $user: $passed/$(($passed + $failed))"
    done

    if [ $failedUsers -gt 0 ]; then
        failedTotal=$(($failedTotal + 1))
        echo
        echo "    ---------------"
        echo "    STEPS FAILED: $failedUsers"
        echo "    ---------------"
    fi
done

testCount=$(echo "$tests" | wc -l)
echo -e "\n=========================="
echo "$(($testCount - $failedTotal))/$testCount tests passed"

if [ $failedTotal -gt 0 ]; then
    echo -e "\nTESTS FAILED: $failedTotal"

    exit 1
fi

exit 0
